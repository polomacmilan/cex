export default {
  /* eslint-disable */
  setGlobalMessage(state, payload) {
    state.globalMessage = payload;
  },
  resetGlobalMessage(state) {
    state.globalMessage = null;
  },
  setCurrencies(state, payload) {
    const arr = Object.keys(payload).map(key => ({label: `${payload[key]} - ${key}`, value:key}));
    state.currencies = arr;
  },
};
