import common from '../api/common';

export default {
  /* eslint-disable */
  async getCurrencies({commit}) {
    try {
      const resp = await common.getCurrencies();
      commit('setCurrencies', resp.data.currencies);
    }
    catch(e) {
      commit('setGlobalMessage', e.response.data.error);
    };
  },
};
