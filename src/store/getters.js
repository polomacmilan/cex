export default {
  /* eslint-disable */
  globalMessage: (state) => {
    return state.globalMessage;
  },
  currencies: (state) => {
    return state.currencies;
  },
};
