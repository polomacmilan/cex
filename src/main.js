import { createApp } from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import App from './App.vue';
import store from './store';

axios.defaults.baseURL = 'https://api.fastforex.io/';

const app = createApp(App);

app.use(store).use(VueAxios, axios).mount('#app');
