import axios from 'axios';

const ak = process.env.VUE_APP_NOT_ACCESS_KEY;

export default {
  getCurrencies() {
    return axios.get(`currencies?api_key=${ak}`);
  },
  convert(params) {
    return axios.get(`fetch-one?from=${params.fromCurr}&to=${params.toCurr}&api_key=${ak}`);
  },
};
