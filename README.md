# Currency coverter

## Description

Currency exchange is executed by entering the amount in the left field
and selecting currencies from the dropdowns. (Dropdowns are searchable).

Operation provides the resulting amount of the exchange in the right field,
which is disabled and serves only for result display.

Deafult currencies are EUR and USD, and default amount is 1.

```
NOTE: Provided API key is valid for next 7 days, counting from 28.10.2021.
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
