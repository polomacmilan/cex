module.exports = {
  css: {
    loaderOptions: {
      scss: {
        prependData: `
          @import "./src/assets/style/variables.scss";
        `,
      },
    },
  },
};
