import { nextTick } from 'vue';
import { mount } from '@vue/test-utils';
import { createStore } from 'vuex';
import Content from '@/App.vue';

describe('App should render component', () => {
  let store 

  beforeEach(() => {
    store = createStore({
      state: {
        currencies: [{'something': 'something'}],
      },
      getters: {
        currencies: (state) => {
          return state.currencies;
        },
        globalMessage: () => {
          return null;
        },
      },
      actions: {
        getCurrencies() {
          return true;
        },
      },
    });
  });

  it('renders multiselect component', () => {
    const comp = mount(Content, {
      global: { plugins: [store] }
    });

    expect(comp.find('.multiselect').exists()).toBe(true);
  });
})