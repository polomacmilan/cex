import { nextTick } from 'vue';
import { mount } from '@vue/test-utils';
import { createStore } from 'vuex';
import FormField from '@/components/InputField.vue';

describe('Testing input events', () => {
  it('emits when input', () => {
    const comp = mount(FormField);
    comp.find('input').trigger('input');

    expect(comp.emitted()).toHaveProperty('update:modelValue');
  });

  it('emits correct value', () => {
    const comp = mount(FormField);
    const input = comp.find('input');
    input.element.value = 100;
    input.trigger('input');

    expect(comp.emitted('update:modelValue')[0]).toEqual(['100']);
  })
})